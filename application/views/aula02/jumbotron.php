<!-- Jumbotron -->
<div class="jumbotron text-center blue-grey lighten-5">

  <!-- Title -->
  <h2 class="card-title h2"><?=$title ?></h2>

  <!-- Subtitle -->
  <p class="indigo-text my-4 font-weight-bold"><?=$descr?></p>

  <!-- Grid row -->
  <div class="row d-flex justify-content-center">

    <!-- Grid column -->
    <div class="col-xl-7 pb-2">

      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga aliquid dolorem ea distinctio exercitationem delectus qui, quas eum architecto, amet quasi accusantium, fugit consequatur ducimus obcaecati numquam molestias hic itaque accusantium doloremque laudantium, totam rem aperiam.</p>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-4 pb-2">

  <button class="btn blue-gradient btn-rounded"><?=$btnu?> <i class="fas fa-upload ml-1"></i></button>
  <button class="btn btn-indigo btn-rounded"><?=$btnd?> <i class="fas fa-download ml-1"></i></button>

</div>
<!-- Jumbotron -->