<?php

defined('BASEPATH') OR exit ('No direct script access allowed');
include_once APPPATH.'libraries/User.php';


class UsuarioModel extends CI_Model{

    public function criar()
    {
        if(sizeof($_POST)== 0) return;
        // $data = $this->input->post();
        // print_r($data);

        $nome = $this->input->post('nome');
        $sobrenome = $this->input->post('sobrenome');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');

        $user = new User($nome,$sobrenome,$email,$senha);
        $user->setTelefone($this->input->post('telefone'));
        $user->save();

    }

    public function lista(){
        $html = '';
        $user = new User();
        //organiza a lista e depois retorna o resultado
        $data =  $user->getAll();

        $html .= '<table class= "table">';

       foreach ($data as $row){
            $html .= '<tr>';
            $html .= '<td>'.$row['nome'].'</td>';
            $html .= '<td>'.$row['sobrenome'].'</td>';
            $html .= '<td>'.$row['email'].'</td>';
            $html .= '<td>'.$row['telefone'].'</td></tr>';
       }
       $html .= '</table>';
       return $html;
    }

}