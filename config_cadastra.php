<?php

require "configManagement.php";

ini_set('display_errors',1);
error_reporting(E_ALL);
header('Content-Type: text/html; charset=utf-8');

$NomeEmpresa = $_POST["NomeEmpresa"];
$DescricaoEmpresa =$_POST["DescricaoEmpresa"];
$TextoEmpresa =$_POST["TextoEmpresa"];

include "db.php";

$configManagement = new configManagement();

$configManagement->set('Empresa.Nome',$NomeEmpresa,$conexao );
$configManagement->set('Empresa.Descricao',$DescricaoEmpresa,$conexao );
$configManagement->set('Empresa.Texto',$TextoEmpresa,$conexao );

header("Location: config.php");
die();

?>