<?php

class configManagement{

    public function __construct()
    {

    }

    public function get($chave,$conexao)
    {
        $query = "select id_config,chave_config,valor_config from tb_config where chave_config = '{$chave}'";
        $resultado = mysqli_query($conexao, $query);
        $num_rows = mysqli_num_rows($resultado);
        if($num_rows == 0)
        {
            $this->set($chave, '',$conexao);
            return '';
        }
        else{
            $dado = mysqli_fetch_assoc($resultado);
            return $dado["valor_config"];
        }
    }

    public function set($chave, $valor,$conexao)
    {
        $query = "select id_config,chave_config,valor_config from tb_config where chave_config = '{$chave}'";
        $resultado = mysqli_query($conexao, $query);
        $num_rows = mysqli_num_rows($resultado);
        if($num_rows == 0)
        {
            $query = "insert into tb_config (chave_config,valor_config) values ('{$chave}','{$valor}') ";
        }
        else{
            $dado = mysqli_fetch_assoc($resultado);
            $id = $dado["id_config"];
            $query = "update tb_config set chave_config = '{$chave}', valor_config = '{$valor}' where id_config = '{$id}'";
        }
        $resultadoDaInsercao = mysqli_query($conexao, $query);
    }
}