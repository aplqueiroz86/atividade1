<?php

require "configManagement.php";

include "db.php";

$configManagement = new configManagement();

$EmpresaNome = $configManagement->get('Empresa.Nome',$conexao );
$EmpresaDescricao = $configManagement->get('Empresa.Descricao',$conexao );
$EmpresaTexto = $configManagement->get('Empresa.Texto',$conexao );

$EmpresaTwitter = $configManagement->get('Empresa.Texto',$conexao );
$EmpresaFacebook = $configManagement->get('Empresa.Texto',$conexao );
$EmpresaEmail = $configManagement->get('Empresa.Texto',$conexao );
?>


<header>
                <div class="collapse bg-dark" id="navbarHeader">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white">Sobre</h4>
                        <p class="text-muted">
                        <?php echo $EmpresaDescricao; ?>
                        </p>
                      </div>
                      <div class="col-sm-4 offset-md-1 py-4">
                        <h4 class="text-white">Contatos</h4>
                        <ul class="list-unstyled">
                            <?php 
                                echo "<li><a href='{$EmpresaTwitter}' class='text-white'><i class='fab fa-twitter'></i> Twitter</a></li>" ;
                                echo "<li><a href='{$EmpresaTwitter}' class='text-white'><i class='fab fa-facebook-square'></i> Facebook</a></li>" ;
                                echo "<li><a href='{$EmpresaTwitter}' class='text-white'><i class='fas fa-envelope'></i> Email</a></li>" ;  
                            ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="navbar navbar-dark bg-dark shadow-sm">
                  <div class="container d-flex justify-content-between">
                    <a href="#" class="navbar-brand d-flex align-items-center">
                      <i class="fas fa-industry"></i> 
                      <strong>
                          <?php echo $EmpresaNome; ?>
                      </strong>
                    </a>
                    <p class="navbar-brand d-flex align-items-center">E-mail do usuario</p>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                  </div>
                </div>
              </header>