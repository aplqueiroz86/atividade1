<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Usuario extends CI_Controller{


    public function index(){
        $this->load->view('commom/header');
        $this->load->view('commom/navbar');
        
        $this->load->model('UsuarioModel', 'model');
        $v['lista'] = $this->model->lista();
        $this->load->view('usuario/table_view', $v);
        
        $this->load->view('commom/footer');
    }



    public function cadastro(){
        $this->load->view('commom/header');
        $this->load->view('commom/navbar');
        
        $this->load->model('UsuarioModel', 'model');
        $this->model->criar();

        $this->load->view('usuario/form_cadastro');
        $this->load->view('commom/footer');
    }
}