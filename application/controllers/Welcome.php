<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function lp2(){
		echo 'Nossa primeira página!';
	}

	public function cadastro(){
		echo 'Código do cadastro!';
	}


	public function busca(){
		echo 'Essa é uma página de busca!';
	}
}
