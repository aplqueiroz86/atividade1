<?php

class Cliente extends CI_Controller{


    
    public function index(){
        $this->load->view('commom/header');
        $this->load->view('commom/navbar');
        $this->load->view('teste');
        $this->load->view('commom/footer');
        
    }

    public function prontuario(){
        echo 'Esta é a página que exibe os dados dos clientes';

    }


    public function cadastro($card,$jumbo){
      
        $this->load->view('commom/header');
        $this->load->view('commom/navbar');
        $v['formulario']= $this->load->view('aula02/form_cadastro', '', true);


        $this->load->model('JumbotronModel');
        $data = $this -> JumbotronModel -> jumbotron_data($jumbo);
        $v['jumbotron']= $this->load->view('aula02/jumbotron', $data, true);

        $this->load->model('CardModel');
        $data = $this -> CardModel -> card_data($card);
        $v['cartao']= $this->load->view('aula02/card', $data, true);


        $v['image']= $this->load->view('aula02/image', '', true);
        $this->load->view('aula02/layout',$v);
        $this->load->view('commom/footer');
    }


    public function editar(){
        echo 'Editar dados do cliente';

    }


}