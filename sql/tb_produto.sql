CREATE TABLE `db_atividade1`.`tb_produto` (
  `id_produto` INT NOT NULL AUTO_INCREMENT,
  `nome_produto` VARCHAR(100) NULL,
  `descricao_produto` VARCHAR(2000) NULL,
  `preco_produto` DECIMAL NULL,
  PRIMARY KEY (`id_produto`));
