<?php 

    ini_set('display_errors',1);
    error_reporting(E_ALL);
    //header('Content-Type: text/html; charset=utf-8');

    include("head.php");
    include("menu.php");

?>

              <main role="main">

                    <section class="jumbotron text-center">
                      <div class="container">
                        <h1 class="jumbotron-heading"><?php echo $EmpresaNome; ?></h1>
                        <p class="lead text-muted"><?php echo $EmpresaTexto; ?></p>
                        <p>
                          <a href="register.html" class="btn btn-primary my-2">Cadastrar</a>
                          <a href="login.html" class="btn btn-secondary my-2">Login</a>
                        </p>
                      </div>
                    </section>
                  
                    <div class="album py-5 bg-light">
                      <div class="container">
                  
                        <div class="row">
                          <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                              <div class="card-body">
                                <p class="card-text">Produto 1</p>
                                <div class="d-flex justify-content-between align-items-center">
                                  <div class="btn-group">
                                    <a href="Produto.html" class="btn btn-sm btn-outline-secondary">Ver</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                  <div class="card-body">
                                    <p class="card-text">Produto 2</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="btn-group">
                                          <a href="Produto.html" class="btn btn-sm btn-outline-secondary">Ver</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-4">
                                    <div class="card mb-4 shadow-sm">
                                      <div class="card-body">
                                        <p class="card-text">Produto 3</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                          <div class="btn-group">
                                              <a href="Produto.html" class="btn btn-sm btn-outline-secondary">Ver</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                        </div>
                      </div>
                    </div>
                  
                  </main>
<?php 
    include("footer.php");
?>