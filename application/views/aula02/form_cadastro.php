


<section class="mb-4">

    <h2 class="h1-responsive font-weight-bold text-center my-4">Entre em Contato</h2>
    <p class="text-center w-responsive mx-auto mb-5">Tem algum dúvida? Por favor, envie sua mensagem.</p>

    <div class="row">

        <div class="col-md-9 mb-md-0 mb-5">
            <form id="formulario-contato" name="formulario-contato" action="mail.php" method="POST">

                <div class="row">

                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="nome" name="nome" class="form-control">
                            <label for="nome" class="">Seu nome</label>
                        </div>
                    </div>
                
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Seu e-mail</label>
                        </div>
                    </div>
                

                </div>
       
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="assunto" name="assunto" class="form-control">
                            <label for="assunto" class="">Assunto</label>
                        </div>
                    </div>
                </div>
          
                <div class="row">

                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="mensagem" name="mensagem" rows="2" class="form-control md-textarea"></textarea>
                            <label for="mensagem">Sua Mensagem</label>
                        </div>

                    </div>
                </div>
            

            </form>

            <div class="text-center text-md-left">
                <a class="btn btn-primary" onclick="document.getElementById('contact-form').submit();">Enviar</a>
            </div>
            <div class="status"></div>


    </div>

</section>


