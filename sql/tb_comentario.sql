CREATE TABLE `db_atividade1`.`tb_comentarios` (
  `id_comentario` INT NOT NULL AUTO_INCREMENT,
  `id_produto` INT NULL,
  `id_user` INT NULL,
  `texto_comentario` VARCHAR(8000) NULL,
  `data_comentario` DATETIME NULL,
  PRIMARY KEY (`id_comentario`),
  INDEX `fk_comentario_user_idx` (`id_user` ASC),
  INDEX `fk_comentario_produto_idx` (`id_produto` ASC),
  CONSTRAINT `fk_comentario_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `db_atividade1`.`tb_user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comentario_produto`
    FOREIGN KEY (`id_produto`)
    REFERENCES `db_atividade1`.`tb_produto` (`id_produto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
