
<div class="container">
    <div class ="row">
        <div class="col-md-8 offset-md-2 ">
     
<form class="text-center border border-light p-5">

    <p class="h4 mb-4">Cadastre-se</p>

    
    <input type="email" id="Email" class="form-control mb-4" placeholder="E-mail">

   <input type="password" id="Senha" class="form-control mb-4" placeholder="Senha">

    <div class="d-flex justify-content-around">
        <div>
            
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="lembrarLogin">
                <label class="custom-control-label" for="lembrarLogin">Lembrar Login</label>
            </div>
        </div>
        <div>
           
            <a href="">Esqueceu sua senha?</a>
        </div>
    </div>

    <button class="btn btn-secondary btn-block my-4" type="submit">Enviar</button>

    <p>Não é membro ainda?
        <a href="">Registre-se</a>
    </p>

   
    <p>ou faça login com:</p>

    <a type="button" class="light-blue-text mx-2" a href="https://www.facebook.com/">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a type="button" class="light-blue-text mx-2" a href="https://twitter.com/login?lang=pt">
        <i class="fab fa-twitter"></i>
    </a>
    <a type="button" class="light-blue-text mx-2" a href="https://br.linkedin.com/">
        <i class="fab fa-linkedin-in"></i>
    </a>
  </form>
</div>
</div>
</div>
