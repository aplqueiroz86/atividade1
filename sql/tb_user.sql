CREATE TABLE `db_atividade1`.`tb_user` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `id_tipo_user` INT NOT NULL,
  `Nome_Usuario` VARCHAR(100) NOT NULL,
  `email_Usuario` VARCHAR(500) NOT NULL,
  `senha_Usuario` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_user`),
  INDEX `fk_user_tipo_user_idx` (`id_tipo_user` ASC),
  CONSTRAINT `fk_user_tipo_user`
    FOREIGN KEY (`id_tipo_user`)
    REFERENCES `db_atividade1`.`tb_tipo_user` (`id_tipo_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);