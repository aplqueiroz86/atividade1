<?php

class User {

    //obrigatório
    private $nome;
    private $sobrenome;
    private $email;
    private $senha;

    //opcional
    private $telefone;

    //variavel para acessar o banco
    private $db;

    function __construct($nome=null, $sobrenome=null, $email=null, $senha=null){
        // echo "construtor: $nome, $sobrenome, $email, $senha<br>";
        $this->nome = $nome;
        $this->sobrenome = $sobrenome;
        $this->email = $email;
        $this->senha = $senha;

        //acesso ao banco de dados pelo CI
        $ci = &get_instance();
        $this->db = $ci->db;

    }


    public function setTelefone($telefone){
        // echo "setter: $telefone<br>";
        $this->telefone = $telefone;
    }

    public function save(){
        $sql = "INSERT INTO user (nome,sobrenome,email,senha,telefone) VALUES ('$this->nome', '$this->sobrenome', '$this->email', '$this->senha', $this->telefone)";
        $this->db->query($sql);
    }

/**
 * Obtém a lista de todos os usuários cadastrados
 * @return associative array
 */

    public function getAll(){
        $sql = "SELECT * FROM user";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

}